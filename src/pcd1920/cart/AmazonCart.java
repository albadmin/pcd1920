package pcd1920.cart;

import java.util.ArrayList;
import java.util.List;
/**
 * This class models an AmazonCart-like reality. Items along with their 
 * frequency are stored inside a List<>.
 * */
public class AmazonCart {

	/**
	 * Internal representation of the cart.
	 * */
	private List<ItemCount> cart = null;
	
	/**
	 * Sole constructor of this class used to build an empty cart.
	 * */
	public AmazonCart() {
		cart = new ArrayList<>();
	}
	
	/**
	 * Adds another item to this class.
	 * 
	 * @param item Used to add an Item to this cart.
	 * 
	 * */
	public void addItem(Item item) {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Returns the cost of the items residing in the cart.
	 * 
	 * @return int Cost of all the items inside the cart. 
	 * 
	 * */
	public int checkOut() {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Returns the internal representation of this cart.
	 * 
	 * @return String Internal representation of this cart returned
	 * 				  as a String object.
	 * 
	 * */
	@Override
	public String toString() {
		throw new UnsupportedOperationException();
	}
}
