package pcd1920.cart;

/**
 * This class wraps an Item object along with its count (number of items) 
 * inside the AmazonCart.
 * */
public class ItemCount {

	/**
	 * Item represented by this ADT
	 * */
	private Item item = null;

	/**
	 * Number of items this ADT represents
	 * */
	private int count = 0;
	
	/**
	 * Total number of this item sold
	 * */
	private static int totCount = 0;

	
	/**
	 * Constructor for this ADT.
	 * */
	public ItemCount(Item item) {}
	
	/**
	 * Constructor for this ADT, fully qualifying all parameters.
	 * */
	public ItemCount(Item item, int cout) {}
	
	
	/**
	 * Increments the number of this particular item represented by 
	 * this ADT
	 * */
	public void incrementCount() {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Decrements the number of this particular item represented by 
	 * this ADT
	 * */
	public void decrementF() {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Returns the cost of all items represented by this ADT
	 * 
	 * @return int representing the cost of all items represented by this 
	 * 				ADT
	 * */
	public int cost() {
		throw new UnsupportedOperationException();
	}
}
