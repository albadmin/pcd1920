package pcd1920.statistics;

public final class WrapperPerformance {

	public static int MAX_RUNS = 20;
	
	public static void main(String args[]) {

		/** Stat. container for String operations */
		long[] int_stats = new long[MAX_RUNS];
		/** Stat. container for StringBuffer operations */
		long[] integer_stats = new long[MAX_RUNS];
		/** Variables containing values of start end time of measurement */
		long start, end;

		for(int run=0; run<MAX_RUNS; run++) {
			
			//append operation for String
			start = System.currentTimeMillis();
			int sum = 0;
			for(int elm=0; elm<Math.pow(10, 4); elm++) {
				sum++;
			}
			end = System.currentTimeMillis();
			int_stats[run] = end-start;

			//append operation for StringBuilder
			start = System.currentTimeMillis();
			Integer sumI = 0;
			for(int elm=0; elm<Math.pow(10, 5); elm++) {
				sumI++;
			}		
			end = System.currentTimeMillis();
			integer_stats[run] = end-start;
		}
		 
		double[] stats = Util.computeSampleStats(int_stats, integer_stats, null);
		System.out.println("StringStats: AVG " + stats[0] + " StdDev " + stats[1]);
		System.out.println("BufferStats: AVG " + stats[2] + " StdDev " + stats[3]);
	}
}
