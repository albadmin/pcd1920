package pcd1920.library;

import java.util.ArrayList;
import java.util.IntSummaryStatistics;

/**
 * This class models the library which is comprised of several shelves holding books each of which identified by some parameters.
 */
public final class Library {

	private final ArrayList<Shelf> library;

	/**
	 * Builds an empty library
	 * */
	public Library() {
		this(new ArrayList<Shelf>());
	}

	/**
	 * Builds a library given some shelves as a input parameter.
	 * 
	 * @param library: shelves to be added to this library
	 * 
	 * */
	public Library(ArrayList<Shelf> library) {
		this.library = cloneInternal(library);
	}
	
	/**
	 * Adds a shelf to the library
	 * 
	 * @param shelf: the shelf
	 */
	public void addShelf(Shelf shelf) {
		library.add(shelf.clone());
	}
	
	/**
	 * @return the total number of books present in this library
	 */
	public int getNumberOfBooksInLibrary() {
		
		return (int)library.stream()
				   		   .flatMap(x -> x.getBooksInShelf().stream())
						   .count();
	}

	/**
	 * @return the book year range (min, max) present in this library.
	 * E.g., if the oldest book year is 1200 and a newly added in 2017, the method returns an array of int {1200, 2017}
	 * */
	public int[] getBooksYearRangeInLibrary() {
		IntSummaryStatistics stats = library.stream()
					  				 		.flatMap(x -> x.getBooksInShelf().stream())
					  				 		.mapToInt(Book::getBookYear)
					  				 		.summaryStatistics();
		return new int[]{
				(stats.getMin() == Integer.MAX_VALUE)? 0:stats.getMin(), 
				(stats.getMax() == Integer.MIN_VALUE)? 0:stats.getMax(), 
				};
	}
	
	/**
	 * @return the average number of books stored in the library shelves.
	 * E.g., Shelf_1 contains 1 and Shelf_1 contains 2 contains 2 => Avg = (1+2)/2
	 */
	public double getAvgNumberOfBookPerShelf() {

		return library.stream()
					  .mapToInt(Shelf::getNumerOfBooksInShelf)
					  .average().orElse(0);

	}

	/**
	 * @return the number of distinct authors present in the library
	 * */
	public int getTotalNumberOfDistinctAuthorsInLibrary() {
		
		return  (int)library.stream()
							.flatMap(x -> x.getBooksInShelf().stream())
							.flatMap(y -> y.getBookAuthors().stream())
							.distinct()
							.count();
	}

	/**
	 * @return the total number of book pages present in this library
	 * */
	public int getTotalNumberOfBookPagesInLibrary() {
		
		  return library.stream()
						.flatMap(x -> x.getBooksInShelf().stream())
						.mapToInt(Book::getBookPages)
						.sum();
	}
	
	
	private ArrayList<Shelf> cloneInternal(ArrayList<Shelf> toCopy) {
		if(toCopy == null) return new ArrayList<>();
		ArrayList<Shelf> copy = new ArrayList<>();
		for(Shelf a: toCopy) 
			copy.add(a.clone());
		return copy;
	}	
}
