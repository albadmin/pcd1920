package pcd1920.immutability;

import pcd1920.immutability.violate.*;

public final class Time {
	private static final int HOURS_PER_DAY = 24;
	private static final int MINUTES_PER_HOUR = 60;

	public final int hour;
	// Not possible to change rep for class
	public final int minute;
	// But we can check invariants, since fields are final

	public Time ( int hour, int minute ) {
		if (hour < 0 || hour >= HOURS_PER_DAY)
			throw new IllegalArgumentException();
		if (minute < 0 || minute >= MINUTES_PER_HOUR)
			throw new IllegalArgumentException();
		this.hour = hour;
		this.minute = minute;
	}
	
	public static void main(String args[]) throws Exception {
		Time today = new Time(11, 0);
		ViolateConstraint.setFinalStatic(today, today.getClass().getField("hour"), 25);
		System.out.println(today.hour);
		//inspectClass();
	}

/**	
	static void inspectClass() {
		
		Constructor<?>[] tConstructors = Time.class.getConstructors();
		for(Constructor<?> c: tConstructors) {
			System.out.println(c.toString());
		}
		Method[] methods = Time.class.getMethods();
		for(Method m: methods) {
			System.out.println(m.toString());
		}
		
	}
*/	
//reminder omitted	
}

