package pcd1920.immutability.violate;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class ViolateConstraint {
	
	public static void setFinalStatic(Object timeObj, 
			Field field, 
			Object newValue) throws Exception {
			
			field.setAccessible(true);
			Field modifiersField = Field.class.getDeclaredField("modifiers");
			modifiersField.setAccessible(true);
			modifiersField.setInt(field,
			field.getModifiers() & ~Modifier.FINAL);
			field.set(timeObj, newValue);
	}
}
